
// F.E.S.E.P.E.R. Software - Ver.0.2
// Here are some parameters to be set
// This will be changend in later version to be configurable on the live system


// The Delay between triggering cameras if the Delay command is used
int cameraDelay= 100;

// How long the trigger should be held 
int shutterOffset = 1000;

// The number of active cameras - adjust according to activeCams[]
static const int cameras = 18;

// The list of cameras in use - needs to be adjusted if cameras are added or removed
int activeCams[cameras]={1,2,3, 5,6,7, 9,10,11, 13,14,15, 17,18,19, 21,22,23};

// The camera groups used for the grouped command
int groups [4][4] = {{1,2,9,10},{3,4,11,12},{5,6,13,14},{7,8,15,16}};

// the number of groups - adjust according to groups[]
int groupCount = 4;

//
//
//

// Private Variables - DO NOT USE if sou dont know what you are doing
long startMilli = 0;
byte maxByte = 255;
byte  triggers[4] = {maxByte,maxByte,maxByte,maxByte};

// Defines the different commands that are available
  enum COM{ALL, DELAY, SETDELAY, NONE, GROUPED};

// The struct is used to enable to match the incoming serial transactions(String) with comands(Enum)
  typedef struct{
    COM c;
    String s;
  }Command;

  static const Command commands[5]{
    {ALL,"ALL"},
    {DELAY,"DELAY"},
    {SETDELAY,"SETDELAY"},
    {NONE,"NONE"},
    {GROUPED,"GROUPED"}
  };

//
//
//
//
//
//

void setup() {
  //Set I/O Registers to output mode
  DDRA = B11111111;
  DDRC = B11111111;
  DDRL = B11111111;
  DDRB = B11111111;

  //Set Output registers to low
  PORTA = B11111111;
  PORTC = B11111111;
  PORTL = B11111111;
  PORTB = B11111111;
  Serial.begin(9600);
}
void loop() {
  serialRead();
  delay(1000);
}

void serialRead(){
  String command = "";
  bool endByte = false;
  
  while(Serial.available() > 0 && !endByte){
    
    byte in = Serial.read();
    //Check if the received character marks the last of the transaction
    
    if(!(in == 10)){
      char input = in;
      command += input;
      }
      else{
        endByte = true;
        }
  }

  
  //Check if the received transaction matches one of the trigger commands
  if(command != ""){
    for(int i=0; i<sizeof(commands)/sizeof(commands[1]); i++){
      if(commands[i].s == command){
        Serial.println("Matched :" + command);
        resolveCommand(commands[i].c);
        break;
      }
      }
    }
  }











void resolveCommand(COM c){
  switch (c){
    
    case ALL: 
      shutterAll();
      break;
      
    case GROUPED:
      shutterGrouped();
      break;
      
    case DELAY:
        shutterDelay();
        break;
        
    default:
      Serial.println("ERROR!!  REVERTED TO DEFAULT");
      break;
    }
  }



  
void shutterAll(){
  int start = millis();
  // Set all I/O Registers high
  PORTA = B00000000;
  PORTC = B00000000;
  PORTL = B00000000;
  PORTB = B00000000;
  
  int endTime = millis();
  int execDelay = endTime-start;
  Serial.print("That took: ");
  Serial.print(execDelay);
  Serial.println(" milliseconds");
  delay(shutterOffset);
  
  //After the set shutter Offset second drop the signals back to low
  PORTA = B11111111;
  PORTC = B11111111;
  PORTL = B11111111;
  PORTB = B11111111;
  } 



void shutterDelay(){
  Serial.println("started delay");
        long loopStart = millis();
        startMilli = millis();
        long endDelay = (cameras*(cameraDelay));
        endDelay += 2 * shutterOffset;
        endDelay += startMilli;
        //Calculate the time needed to finish the controll loop an run the Delay control loop for this duration
        
        while(millis() < endDelay){
          setArraysDelay();
          setRegs();       
          }
          
        long loopDelay = millis()-loopStart;
        Serial.print("That took: ");
        Serial.print(loopDelay);
        Serial.println(" milliseconds");
  }

void shutterGrouped(){
  Serial.println("started GROUP");
      long loopStartG = millis();
      startMilli = millis();
      long endGrouped = groupCount*cameraDelay;
      endGrouped += 2* shutterOffset;
      endGrouped += startMilli;
      //Calculate the time needed to finish the controll loop an run the Grouped control loop for this duration
 
      while(millis() < endGrouped){
        setArraysGrouped();
        setRegs();       
        }
        
      int loopDelayG = millis()-loopStartG;
      Serial.print("That took: ");
      Serial.print(loopDelayG);
      Serial.println(" milliseconds");
  }


void setArraysGrouped(){
  // Check for each group from groups[] if it should be trigerred at this moment
  
  for(int i=0; i < groupCount; i++){
    int myStart = i*cameraDelay;
    int currStartTime = millis()-startMilli;
    int myEnd = myStart + shutterOffset;
    
    if(myStart <= currStartTime && myEnd >= currStartTime){
      // If the group should be triggered set all corresponding camera registers to high
      
      for(int a =0; a < (sizeof(groups[i])/sizeof(groups[i][0])); a++){
        int arrayPos = (groups[i][a]-1)/8;
        bitClear(triggers[arrayPos],(groups[i][a]-1)%8);
        }
        
      }else{
        // If the group should not be triggered set all corresponding camera registers to low
        
        for(int a =0; a < (sizeof(groups[i])/sizeof(groups[i][0])); a++){
          int arrayPos = (groups[i][a]-1)/8;
          bitSet(triggers[arrayPos],(groups[i][a]-1)%8);
          }
        }
    }
  
  }






void setArraysDelay(){
  //Check if all 32 cameras are in use or less
  
  if(cameras < 32){
    //If less cameras are used set all trigger signals to low
    triggers[0] = maxByte;
    triggers[2] = maxByte;
    triggers[3] = maxByte;
    triggers[4] = maxByte;
      
      for(int i=0; i < activeCams; i++){
       // Check for each camera in activeCams[] if it should be triggered
       
       int myStart = i*cameraDelay;
       int currStartTime = millis()-startMilli;
       int myEnd = myStart + shutterOffset;
       int Pos = activeCams[i];
       int arrayPos = Pos/8;
       
       if(myStart <= currStartTime && myEnd >= currStartTime){
        // If it should be triggerde set the corresponding output bit to high
         bitClear(triggers[arrayPos],Pos%8);
       } 
      }
   }else{
    
    for(int i = 0; i < cameras; i++){
      int myStart = i*cameraDelay;
      int currStartTime = millis()-startMilli;
      int myEnd = myStart + shutterOffset;
      int arrayPos = i/8;
      
      if(myStart <= currStartTime && myEnd >= currStartTime){
        // If it should be triggerde set the corresponding output bit to high, if not it will be set to low
        bitClear(triggers[arrayPos],i%8);
      }else{bitSet(triggers[arrayPos],i%8);}
    }
   }
}

void setRegs(){
  PORTA = triggers[0];
  PORTC = triggers[1];
  PORTL = triggers[2];
  PORTB = triggers[3];  
  }
