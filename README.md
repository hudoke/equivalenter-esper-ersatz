# F.E.S.P.E.R - Finalized Equivalent performing System to Professionel  EspeR solution

F.E.S.P.E.R. ist ein System das als Alternative zum professionellen Triggersystem der Firma Esper entwickelt wurde.
Die momentane Version der Hard- und Software unterstützt die Stromversorgung, Datenextraktion und das Auslösen von bis zu 32 Kameras.
Als Herzstück des Systems agiert ein Arduino Mega der mit Hilfe des Codes aus diesem Repository verschiedene Auslösenmodi unterstützt.

## Dokumentation Hardware

* Dokumentation der Hardware für dieses Projekt ist zu finden unter "TODO"
* Triggerdata Box (TDB) ist mit 8 USB und 8 2,5mm 3 pol Klinken Buchsen ausgestattet.
* Power Box liefert bis zu 6A bei 7,4V für je 2 x 4 Kameras
* TDB ist mit RJ45 Port ausgestattet um mit dem Arduino verbunden zu werden
* Die 8 Adern werden genutzt um Signale ein acht Kanal solid state Relay anzusteuern
* Stromversorgung und Ground für die Signalleitungen sind über eins der USB hubs gelöst, daher müssen Arduino und alle TDBs am gleichen Computer (ggf. sogar USB Controller) angeschlossen sein.
* Für den Signalground  wird an einer Alternativlösung gearbeitet 

## Funktionsweise Software
* Dreigeteilt
* Befehle werden über serielle Schnittstelle empfangen und danach ausgewertet
* Je nach Befehl wird unterschiedliche Logik verwendet um festzulegen die outputbytes zu schreiben
* Am ende jedes logik zykles werden die outputbytes mittels port manipulation in die entsprechenden Register geschrieben



